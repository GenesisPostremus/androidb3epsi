package com.example.projetepsi;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projetepsi.model.Produit;
import com.squareup.picasso.Picasso;

public class DetailProduit extends AppCompatActivity {

    private Produit produit;

    public static void display(AppCompatActivity activity, Produit produit) {
        Intent intent = new Intent(activity, DetailProduit.class);
        intent.putExtra("produit", produit);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_product_detail);
        produit = (Produit) getIntent().getExtras().get("produit");
        TextView desc = findViewById(R.id.textViewProductDesc);
        TextView headerTitle = findViewById(R.id.headerTitle);
        ImageView imageViewDetails = findViewById(R.id.imageViewProductDetails);

        Picasso.get().load(produit.getPicture_url()).into(imageViewDetails);
        headerTitle.setText(produit.getName());
        desc.setText(produit.getDescription());

        setTitle(produit.getName());
    }
}
