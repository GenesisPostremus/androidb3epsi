package com.example.projetepsi.model;

import java.io.Serializable;

public class Student implements Serializable {
    private String nom;
    private String image;
    private String email;
    private String groupe;

    public Student(String image, String nom, String email, String groupe) {
        this.image = image;
        this.nom = nom;
        this.email = email;
        this.groupe = groupe;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGroupe() {
        return groupe;
    }

    public void setGroupe(String groupe) {
        this.groupe = groupe;
    }
}
