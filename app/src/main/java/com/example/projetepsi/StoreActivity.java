package com.example.projetepsi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projetepsi.model.Rayon;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.List;

public class StoreActivity extends AppCompatActivity {

    private RayonAdapter rayonAdapter;
    private List<Rayon> rayons = new ArrayList<>();

    public static void display(AppCompatActivity activity) {
        Intent intent = new Intent(activity, StoreActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_store_list);
        TextView headerTitle = findViewById(R.id.headerTitle);
        headerTitle.setText("Rayons");
        final ListView listView = findViewById(R.id.listViewRayons);
        rayonAdapter = new RayonAdapter(this, R.layout.c_store_list, rayons);
        listView.setAdapter(rayonAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DetailRayon.display(StoreActivity.this, rayons.get(position));
            }
        });
        String urlStr = "http://djemam.com/epsi/categories.json";
        new HttpAsyTask(urlStr, new HttpAsyTask.HttpAsyTaskListener() {
            @Override
            public void webServiceDone(String result) {
                initData(result);
            }

            @Override
            public void webServiceError(Exception e) {
                e.getMessage();
            }
        }).execute();
    }

    private void initData(String data) {
        try {
            Gson gson = new Gson();
            JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
            JsonArray jsonElements = (JsonArray) jsonObject.get("items");
            for (int i = 0; i < jsonElements.size(); i++) {
                Rayon rayon = gson.fromJson(jsonElements.get(i), Rayon.class);
                rayons.add(rayon);
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        rayonAdapter.notifyDataSetChanged();
    }
}
