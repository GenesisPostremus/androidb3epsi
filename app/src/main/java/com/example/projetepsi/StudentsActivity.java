package com.example.projetepsi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projetepsi.model.Student;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StudentsActivity extends AppCompatActivity {

    private StudentAdapter studentAdapter;
    private List<Student> students = new ArrayList<>();

    public static void display(AppCompatActivity activity) {
        Intent intent = new Intent(activity, StudentsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_info_students);
        TextView headerTitle = findViewById(R.id.headerTitle);
        headerTitle.setText("Infos");
        final ListView listView = findViewById(R.id.listViewStudents);
        try {
            initData();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        studentAdapter = new StudentAdapter(this, R.layout.c_student, students);
        listView.setAdapter(studentAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DetailStudent.display(StudentsActivity.this, students.get(position));
            }
        });
    }

    private void initData() throws IOException, ParseException {
        try {
            Gson g = new Gson();
            String studentsData = "[{ image : 'https://www.poledesetoiles.fr/wp-content/uploads/2017/04/pluto-1315109_1920.jpg', nom : 'bernard dupont', email : 'bernard.dupont@aze.fr', groupe : 'B3'}, " +
                    "               { image : 'https://www.stelvision.com/astro/wp-content/uploads/2019/07/Neptune2_credit_Nasa_JPL.jpg', nom : 'jean champs', email : 'jean.champs@aze.fr', groupe : 'B3'}]";
            students = Arrays.asList(g.fromJson(studentsData, Student[].class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
