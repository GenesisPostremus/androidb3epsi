package com.example.projetepsi;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.buttonStore).setOnClickListener(this);
        findViewById(R.id.buttonStudent).setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonStudent:
                StudentsActivity.display(MainActivity.this);
                break;
            case R.id.buttonStore:
                StoreActivity.display(MainActivity.this);
                break;
        }
    }


    @Override
    public void onBackPressed() {
    }
}
