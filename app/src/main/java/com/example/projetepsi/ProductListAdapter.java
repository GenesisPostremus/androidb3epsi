package com.example.projetepsi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.projetepsi.model.Produit;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductListAdapter extends ArrayAdapter<Produit> {

    public ProductListAdapter(@NonNull Context context, int resource, @NonNull List<Produit> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.c_product, null);

        ImageView imageView = convertView.findViewById(R.id.imageViewProduct);
        TextView name = convertView.findViewById(R.id.textViewNameProduct);
        TextView desc = convertView.findViewById(R.id.textViewDesc);

        Produit produit = getItem(position);

        Picasso.get().load(produit.getPicture_url()).into(imageView);
        name.setText(produit.getName());
        desc.setText(produit.getDescription());

        return convertView;
    }
}
