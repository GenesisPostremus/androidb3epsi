package com.example.projetepsi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.projetepsi.model.Rayon;

import java.util.List;

public class RayonAdapter extends ArrayAdapter<Rayon> {
    public RayonAdapter(@NonNull Context context, int resource, @NonNull List<Rayon> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.c_store_list, null);

        TextView rayonName = convertView.findViewById(R.id.textViewRayon);

        Rayon rayon = getItem(position);

        rayonName.setText(rayon.getTitle());
        return convertView;
    }
}
