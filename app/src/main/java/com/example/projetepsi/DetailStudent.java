package com.example.projetepsi;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projetepsi.model.Student;
import com.squareup.picasso.Picasso;

public class DetailStudent extends AppCompatActivity {

    private Student student;

    public static void display(AppCompatActivity activity, Student student) {
        Intent intent = new Intent(activity, DetailStudent.class);
        intent.putExtra("student", student);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_detail_student);
        student = (Student) getIntent().getExtras().get("student");
        TextView textName = findViewById(R.id.textViewNameDetails);
        TextView textEmail = findViewById(R.id.textViewEmailDetails);
        TextView textGroup = findViewById(R.id.textViewGroupDetail);
        TextView headerTitle = findViewById(R.id.headerTitle);
        ImageView imageViewDetails = findViewById(R.id.imageViewStudentDetails);

        textName.setText(student.getNom());
        textEmail.setText(student.getEmail());
        textGroup.setText(student.getGroupe());
        Picasso.get().load(student.getImage()).into(imageViewDetails);
        headerTitle.setText(student.getNom());

        setTitle(student.getNom());
    }

}
