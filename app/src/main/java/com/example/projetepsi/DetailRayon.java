package com.example.projetepsi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projetepsi.model.Produit;
import com.example.projetepsi.model.Rayon;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.List;

public class DetailRayon extends AppCompatActivity {

    private ProductListAdapter productListAdapter;
    private List<Produit> produits = new ArrayList<>();
    private Rayon rayon;

    public static void display(AppCompatActivity activity, Rayon rayon) {
        Intent intent = new Intent(activity, DetailRayon.class);
        intent.putExtra("rayon", rayon);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_products_rayon);
        rayon = (Rayon) getIntent().getExtras().get("rayon");
        TextView headerTitle = findViewById(R.id.headerTitle);
        headerTitle.setText(rayon.getTitle());
        final ListView listView = findViewById(R.id.listViewProducts);
        productListAdapter = new ProductListAdapter(this, R.layout.c_product, produits);
        listView.setAdapter(productListAdapter);
        new HttpAsyTask(rayon.getProducts_url(), new HttpAsyTask.HttpAsyTaskListener() {
            @Override
            public void webServiceDone(String result) {
                initData(result);
            }

            @Override
            public void webServiceError(Exception e) {
                e.getMessage();
            }
        }).execute();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DetailProduit.display(DetailRayon.this, produits.get(position));
            }
        });
    }

    private void initData(String data) {
        try {
            Gson gson = new Gson();
            JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
            JsonArray jsonElements = (JsonArray) jsonObject.get("items");
            for (int i = 0; i < jsonElements.size(); i++) {
                Produit produit = gson.fromJson(jsonElements.get(i), Produit.class);
                produits.add(produit);
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        productListAdapter.notifyDataSetChanged();
    }
}
